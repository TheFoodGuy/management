import spock.lang.Specification

class SanityTest extends Specification {
    def "one plus one should equal two or the sanity test"() {
        expect:
        1 + 1 == 2
    }

    def "two plus two should equal four or the second sanity test"() {
        given:
            int left = 2
            int right = 22

        when:
            int result = left + right

        then:
            result == 24
    }

    def "should be able to remove from list"() {
        given:
            def list = [1,2,3,4]

        when:
            list.remove(0)

        then:
            list == [2,3,4]
    }

    def "should get an index out of bounds when removing a non-existent item"() {
        given:
            def list = [1,2,3,4]

        when:
            list.remove(20)

        then:
            thrown(IndexOutOfBoundsException)
            list.size() == 4
    }
}
