import com.example.managementsvc.controller.UserController
import com.example.managementsvc.entity.UserEntity
import com.example.managementsvc.service.UserService
import com.fasterxml.jackson.databind.ObjectMapper
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

@SpringBootTest(classes = [UserController])
class UserControllerTest extends Specification{
    @Autowired
    private MockMvc mvc

    @SpringBean
    UserService userService = Mock()

    @Autowired
    ObjectMapper objectMapper

//    def "when get is performed"() {
//        given: "a user with example first, last, and user names"
//        UserEntity testUser1 = new UserEntity()
//        testUser1.setLastName("Liang")
//        testUser1.setLastName("David")
//        testUser1.setUsername("david.liang")
//
//        and: "a com.example.managementsvc.service that returns this user"
//        userService.findAll() >> [testUser1].asList()
//
//        when: "a get request of findAll is called"
//        def results = mvc.perform(get("/users"))
//
//        then:
//        results.andExpect(status().isOk())
//    }
}
