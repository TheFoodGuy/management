DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) NOT NULL,
    `last_name` varchar(255) NOT NULL,
    `username` varchar(255) NOT NULL,
    PRIMARY KEY(`id`)
);

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `description` TINYTEXT NULL,
    `assignee_id` bigint(20) NOT NULL,
    `type` varchar(255) NULL,
    PRIMARY KEY(`id`)
);

DROP TABLE IF EXISTS `board`;
CREATE TABLE `board` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    PRIMARY KEY(`id`)
);