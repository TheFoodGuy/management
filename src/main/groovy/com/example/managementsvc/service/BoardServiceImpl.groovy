package com.example.managementsvc.service

import com.example.managementsvc.entity.BoardEntity
import com.example.managementsvc.repository.BoardRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BoardServiceImpl implements BoardService{

    @Autowired
    BoardRepository boardRepository

    @Override
    List<BoardEntity> findAll() {
        return boardRepository.findAll()
    }

    @Override
    BoardEntity findById(long id) {
        return boardRepository.findById(id)
    }

    @Override
    BoardEntity saveBoard(BoardEntity board) {
        return boardRepository.save(board)
    }

    @Override
    BoardEntity updateBoard(BoardEntity board) {
        return boardRepository.save(board)
    }

    @Override
    BoardEntity deleteBoard(long id) {
        return boardRepository.deleteById(id)
    }
}
