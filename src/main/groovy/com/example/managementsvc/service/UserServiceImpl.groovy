package com.example.managementsvc.service

import com.example.managementsvc.entity.UserEntity
import com.example.managementsvc.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service()
class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository

    @Override
    List<UserEntity> findAll() {
        return userRepository.findAll()
    }

    @Override
    UserEntity findById(long id) {
        return userRepository.findById(id) as UserEntity
    }

    @Override
    UserEntity saveUser(UserEntity user) {
        return userRepository.save(user)
    }

    @Override
    UserEntity updateUser(UserEntity user) {
        return userRepository.save(user)
    }

    @Override
    UserEntity deleteUser(long id) {
        return userRepository.deleteById(id)
    }
}
