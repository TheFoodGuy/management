package com.example.managementsvc.service

import com.example.managementsvc.entity.TaskEntity
import com.example.managementsvc.repository.TaskRepository
import org.springframework.beans.factory.annotation.Autowired

class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepository taskRepository

    @Override
    List<TaskEntity> findAll() {
        return taskRepository.findAll()
    }

    @Override
    TaskEntity findById(long id) {
        return taskRepository.findById(id) as TaskEntity
    }

    @Override
    TaskEntity saveTask(TaskEntity task) {
        return taskRepository.save(task)
    }

    @Override
    TaskEntity updateTask(TaskEntity task) {
        return taskRepository.save(task)
    }

    @Override
    TaskEntity deleteTask(long id) {
        return taskRepository.deleteById(id)
    }
}
