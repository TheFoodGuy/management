package com.example.managementsvc.service

import com.example.managementsvc.entity.UserEntity


interface UserService {

    List<UserEntity> findAll()

    UserEntity findById(long id)

    UserEntity saveUser(UserEntity user)

    UserEntity updateUser(UserEntity user)

    UserEntity deleteUser(long id)

}
