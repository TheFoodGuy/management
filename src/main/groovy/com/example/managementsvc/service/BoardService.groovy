package com.example.managementsvc.service

import com.example.managementsvc.entity.BoardEntity

interface BoardService {
    List<BoardEntity> findAll()

    BoardEntity findById(long id)

    BoardEntity saveBoard(BoardEntity board)

    BoardEntity updateBoard(BoardEntity board)

    BoardEntity deleteBoard(long id)
}