package com.example.managementsvc.service

import com.example.managementsvc.entity.TaskEntity


interface TaskService {
    List<TaskEntity> findAll()

    TaskEntity findById(long id)

    TaskEntity saveTask(TaskEntity task)

    TaskEntity updateTask(TaskEntity task)

    TaskEntity deleteTask(long id)
}