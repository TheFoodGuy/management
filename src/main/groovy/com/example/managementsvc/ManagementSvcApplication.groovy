package com.example.managementsvc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
class ManagementSvcApplication {

    static void main(String[] args) {
        SpringApplication.run(ManagementSvcApplication, args)
    }

}
