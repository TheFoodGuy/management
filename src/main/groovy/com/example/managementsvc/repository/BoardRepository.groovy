package com.example.managementsvc.repository

import com.example.managementsvc.entity.BoardEntity
import org.springframework.data.jpa.repository.JpaRepository

interface BoardRepository extends JpaRepository<BoardEntity, long>{

}