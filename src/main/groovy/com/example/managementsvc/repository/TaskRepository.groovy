package com.example.managementsvc.repository

import com.example.managementsvc.entity.TaskEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TaskRepository extends JpaRepository<TaskEntity, long> {

}