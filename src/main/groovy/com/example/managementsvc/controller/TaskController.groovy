package com.example.managementsvc.controller

import com.example.managementsvc.entity.TaskEntity
import com.example.managementsvc.service.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping('/tasks')
class TaskController {

    @Autowired
    TaskService taskService

    @GetMapping
    List<TaskEntity> getAllTasks() {
        taskService.findAll()
    }

    @GetMapping('/{taskId}')
    TaskEntity getTaskById(@PathVariable long id) {
        taskService.getTaskById(id)
    }
}
