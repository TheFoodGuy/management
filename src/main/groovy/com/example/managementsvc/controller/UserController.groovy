package com.example.managementsvc.controller

import com.example.managementsvc.entity.UserEntity
import com.example.managementsvc.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping('/users')
class UserController {

    @Autowired
    UserService userService

    @GetMapping
    List<UserEntity> getAllUsers() {
        userService.findAll()
    }

    @PostMapping
    UserEntity saveUser(@RequestBody UserEntity userRequest) {
        userService.saveUser(userRequest)
    }

    @PutMapping
    UserEntity updateUser(@RequestBody UserEntity userRequest) {
        userService.saveUser(userRequest)
    }

    @DeleteMapping('/{userId}')
    deleteUser(@PathVariable long userId) {
        userService.deleteUser(userId)
    }

    @GetMapping('/{userId}')
    UserEntity getUserById(@PathVariable long userId) {
        userService.findById(userId)
    }
}
