package com.example.managementsvc.entity

import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

class TaskEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id

    @Column(nullable = false)
    private String title
    private String description
    private long assignee_id
    private String type
}
