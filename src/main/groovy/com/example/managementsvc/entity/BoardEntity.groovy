package com.example.managementsvc.entity

import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

class BoardEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id

    @Column(nullable = false)
    private String name

    long getId() {
        return id
    }

    void setId(long id) {
        this.id = id
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }
}
